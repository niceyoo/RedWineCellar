# RedWineCellar

### 介绍

博客园 **netty** 红酒窖案例demo

链接：[https://www.cnblogs.com/niceyoo/p/13269756.html](https://www.cnblogs.com/niceyoo/p/13269756.html)

### 说明

netty-client：客户端

netty-server：服务端

完成对红酒窖的室内温度采集及监控功能。由本地应用程序+温度传感器定时采集室内温度上报至服务器，如果温度 >20 °C 则由服务器下发重启空调指令，如果本地应用长时间不上传温度给服务器，则给户主手机发送一条预警短信。


