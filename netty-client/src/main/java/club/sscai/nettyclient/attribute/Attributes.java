package club.sscai.nettyclient.attribute;

import club.sscai.nettyclient.session.Session;
import io.netty.util.AttributeKey;

public interface Attributes {

    AttributeKey<Session> SESSION = AttributeKey.newInstance("session");

}
