package club.sscai.nettyclient.client;

import club.sscai.nettyclient.codec.PacketDecoder;
import club.sscai.nettyclient.codec.PacketEncoder;
//import club.sscai.nettyclient.handle.HeartbeatHandler;
import club.sscai.nettyclient.codec.SpliterFilter;
import club.sscai.nettyclient.handle.LoginResponseHandler;
import club.sscai.nettyclient.handle.MessageResponseHandler;
import club.sscai.nettyclient.handle.NettyClientHandler;
import club.sscai.nettyclient.protocol.request.LoginRequestPacket;
import club.sscai.nettyclient.protocol.request.MessageRequestPacket;
import club.sscai.nettyclient.util.SessionUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * 客户端
 */
public class NettyClient {

    private static String HOST = "127.0.0.1";
    private static final int PORT = 8070;
    private static int MAX_RETRY = 5;


    public static void main(String[] args) {
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();
        bootstrap
            // 1.指定线程模型
            .group(workerGroup)
            // 2.指定 IO 类型为 NIO
            .channel(NioSocketChannel.class)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .option(ChannelOption.TCP_NODELAY, true)
            // 3.IO 处理逻辑
            .handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) {
                ch.pipeline()
                    .addLast(new IdleStateHandler(0, 10, 0))
                        //.addLast(new StringDecoder())
                        //.addLast(new StringEncoder())
                        // 心跳检测
                        //.addLast(new HeartbeatHandler())
                        //.addLast(new NettyClientHandler())
                        // 消息协议过滤器
                        .addLast(new SpliterFilter())
                        // 消息编码
                        .addLast(new PacketDecoder())
                        // 设备登陆
                        .addLast(new LoginResponseHandler())
                        // 消息处理
                        .addLast(new MessageResponseHandler())
                        // 消息解码
                        .addLast(new PacketEncoder());
                }
            });
        // 4.建立连接
        bootstrap.connect(HOST, PORT).addListener(future -> {
            if (future.isSuccess()) {
                System.out.println("连接成功!");
            } else {
                System.err.println("连接失败!");
                connect(bootstrap, HOST, PORT, MAX_RETRY);
            }

        });
    }

    /**
     * 失败重连
     * @param bootstrap
     * @param host
     * @param port
     * @param retry
     */
    private static void connect(Bootstrap bootstrap, String host, int port, int retry) {
        bootstrap.connect(host, port).addListener(future -> {
            if (future.isSuccess()) {
                System.out.println("连接成功!");
                // 启动一个线程\模拟通信
                // Channel channel = ((ChannelFuture) future).channel();
                // startConsoleThread(channel);
            } else if (retry == 0) {
                System.err.println("重试次数已用完，放弃连接！");
            } else {
                // 第几次重连
                int order = (MAX_RETRY - retry) + 1;
                // 本次重连的间隔
                int delay = 1 << order;
                System.err.println(new Date() + ": 连接失败，第" + order + "次重连……");
                bootstrap.config().group().schedule(() ->
                        connect(bootstrap, host, port, retry - 1), delay, TimeUnit.SECONDS);
            }
        });
    }

}
