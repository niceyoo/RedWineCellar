package club.sscai.nettyclient.controller;

import club.sscai.nettyclient.protocol.request.MessageRequestPacket;
import club.sscai.nettyclient.util.SessionUtil;
import io.netty.channel.Channel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientController {

    /**
     * 给客户端发送测试消息
     *
     * @return
     */
    @GetMapping("/sendServerMsg")
    public String sendClientMsg() {
        Channel channel = SessionUtil.getChannel("niceyoo");
        // 4.将消息发送给消息接收方
        if (channel != null && SessionUtil.hasLogin(channel)) {
            MessageRequestPacket messageRequestPacket = new MessageRequestPacket();
            messageRequestPacket.setSn("3ds6gdutcbwd45hu6d301");
            messageRequestPacket.setMessage("我是客户端的消息");
            channel.writeAndFlush(messageRequestPacket);
            System.out.println("客户端消息已发送...");
        } else {
            System.err.println("[niceyoo] 不在线，发送失败!");
        }
        return "send ok";
    }

}
