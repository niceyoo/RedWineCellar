package club.sscai.nettyclient.handle;

import club.sscai.nettyclient.protocol.request.LoginRequestPacket;
import club.sscai.nettyclient.protocol.response.DeviceConfig;
import club.sscai.nettyclient.protocol.response.LoginResponsePacket;
import club.sscai.nettyclient.session.Session;
import club.sscai.nettyclient.util.SessionUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.Date;

/**
 * 登陆请求逻辑处理类
 * 登陆成功后获取设备配置信息
 */
public class LoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        System.out.println("-------login-------");
        // 1、创建登录对象「手动写死设备序列号」
        LoginRequestPacket loginRequestPacket = new LoginRequestPacket();
        loginRequestPacket.setSn("3ds6gdutcbwd45hu6d301");
        loginRequestPacket.setVer(1);
        // 2、写数据
        ctx.channel().writeAndFlush(loginRequestPacket);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginResponsePacket loginResponsePacket) {
        if (loginResponsePacket.isSuccess()) {
            System.out.println(new Date() + ": 客户端登录成功");
            // 读取从服务端获取的配置参数
            DeviceConfig config = loginResponsePacket.getConfig();
            // TODO 更新硬件设备中的音量大小
            Integer deviceMusicSize = config.getDeviceMusicSize();
            System.out.println("最新设备音量大小：" + deviceMusicSize);
            System.out.println("当前设备最新名称：" + config.getName());
            // 更新本地登录状态
            SessionUtil.bindSession(new Session(loginResponsePacket.getServiceId()), ctx.channel());
        } else {
            System.out.println(new Date() + ": 客户端登录失败，原因：" + loginResponsePacket.getReason());
        }
    }
}
