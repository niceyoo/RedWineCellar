package club.sscai.nettyclient.handle;

import club.sscai.nettyclient.protocol.Packet;
import club.sscai.nettyclient.protocol.PacketCodeC;
import club.sscai.nettyclient.protocol.request.LoginRequestPacket;
import club.sscai.nettyclient.protocol.response.LoginResponsePacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.time.LocalTime;
import java.util.Date;

/**
 * 客户端业务逻辑处理类
 */
public class NettyClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        System.out.println(LocalTime.now() + ": 客户端登陆验证...");

        // 1、创建登陆请求对象
        LoginRequestPacket loginRequestPacket = new LoginRequestPacket();
//        loginRequestPacket.setUserId(UUID.randomUUID().toString());
//        loginRequestPacket.setUsername("flash");
//        loginRequestPacket.setPassword("pwd");

        // 2、对登陆对象二进制编码
//        ByteBuf buffer = PacketCodeC.INSTANCE.encode(ctx.alloc(), loginRequestPacket);
//
//        // 3、给服务端发送数据
//        ctx.channel().writeAndFlush(buffer);
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        // 1、将收到的消息转换成ByteBuf对象
        ByteBuf byteBuf = (ByteBuf) msg;
        // 2、解码为java对象
        Packet packet = PacketCodeC.INSTANCE.decode(byteBuf);
        // 3、判断消息是否为登陆
        if (packet instanceof LoginResponsePacket) {
            LoginResponsePacket loginResponsePacket = (LoginResponsePacket) packet;

            if (loginResponsePacket.isSuccess()) {
                // 设置成功登陆标志
//                LoginUtil.markAsLogin(ctx.channel());
                System.out.println(new Date() + ": 客户端登录成功");
            } else {
                System.out.println(new Date() + ": 客户端登录失败，原因：" + loginResponsePacket.getReason());
            }
        }
    }

}
