package club.sscai.nettyclient.protocol.command;

/**
 * 公共参数
 */
public interface Command {

    /**
     * 登陆请求
     */
    Byte LOGIN_REQUEST = 1;

    /**
     * 登陆回复
     */
    Byte LOGIN_RESPONSE = 2;

    /**
     * 心跳请求
     */
    Byte HEARTBEAT_REQUEST = 3;

    /**
     * 消息请求
     */
    Byte MESSAGE_REQUEST = 4;

    /**
     * 消息回复
     */
    Byte MESSAGE_RESPONSE = 5;

    /**
     * 所有设备加入的群组号码
     */
    int GROUP_NUMBER = 10086;

}
