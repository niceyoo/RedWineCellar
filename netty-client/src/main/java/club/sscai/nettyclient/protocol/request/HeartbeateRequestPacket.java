package club.sscai.nettyclient.protocol.request;

import club.sscai.nettyclient.protocol.Packet;
import lombok.Data;

import static club.sscai.nettyclient.protocol.command.Command.HEARTBEAT_REQUEST;

@Data
public class HeartbeateRequestPacket extends Packet {

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 消息内容
     */
    private String msg;

    @Override
    public Byte getCommand() {
        return HEARTBEAT_REQUEST;
    }
}
