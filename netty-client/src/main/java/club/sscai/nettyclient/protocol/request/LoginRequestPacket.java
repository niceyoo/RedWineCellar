package club.sscai.nettyclient.protocol.request;

import club.sscai.nettyclient.protocol.Packet;
import lombok.Data;
import static club.sscai.nettyclient.protocol.command.Command.LOGIN_REQUEST;

@Data
public class LoginRequestPacket extends Packet {

    /**
     * 协议版本号，目前为止1
     */
    private Integer ver;

    /**
     * 设备序列号
     */
    private String sn;

    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}
