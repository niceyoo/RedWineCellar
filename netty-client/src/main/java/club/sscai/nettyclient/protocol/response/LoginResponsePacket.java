package club.sscai.nettyclient.protocol.response;

import lombok.Data;
import club.sscai.nettyclient.protocol.Packet;
import static club.sscai.nettyclient.protocol.command.Command.LOGIN_RESPONSE;

@Data
public class LoginResponsePacket extends Packet {

    private String serviceId;

    private boolean success;

    private String reason;

    private DeviceConfig config;

    @Override
    public Byte getCommand() {
        return LOGIN_RESPONSE;
    }
}
