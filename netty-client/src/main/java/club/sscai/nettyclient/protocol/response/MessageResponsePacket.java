package club.sscai.nettyclient.protocol.response;

import lombok.Data;
import club.sscai.nettyclient.protocol.Packet;
import static club.sscai.nettyclient.protocol.command.Command.MESSAGE_RESPONSE;

@Data
public class MessageResponsePacket extends Packet {

    /**
     * 设备序列号
     */
    private String sn;

    /**
     * 消息内容
     */
    private String message;

    @Override
    public Byte getCommand() {
        return MESSAGE_RESPONSE;
    }
}
