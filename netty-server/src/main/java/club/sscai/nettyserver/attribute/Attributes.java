package club.sscai.nettyserver.attribute;

import club.sscai.nettyserver.session.Session;
import io.netty.util.AttributeKey;

public interface Attributes {

    AttributeKey<Session> SESSION = AttributeKey.newInstance("session");

}
