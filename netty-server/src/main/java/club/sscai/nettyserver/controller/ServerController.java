package club.sscai.nettyserver.controller;

import club.sscai.nettyserver.protocol.request.MessageRequestPacket;
import club.sscai.nettyserver.protocol.response.MessageResponsePacket;
import club.sscai.nettyserver.util.SessionUtil;
import io.netty.channel.Channel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/server")
public class ServerController {

    /**
     * 给客户端发送测试消息
     *
     * @return
     */
    @GetMapping("/sendClientMsg")
    public String sendClientMsg() {
        Channel channel = SessionUtil.getChannel("3ds6gdutcbwd45hu6d301");
        // 4.将消息发送给消息接收方
        if (channel != null && SessionUtil.hasLogin(channel)) {
            MessageResponsePacket messageResponsePacket = new MessageResponsePacket();
            messageResponsePacket.setSn("niceyoo");
            messageResponsePacket.setMessage("我是服务端的消息");
            channel.writeAndFlush(messageResponsePacket);
            System.out.println("服务端消息已发送...");
        } else {
            System.err.println("[3ds6gdutcbwd45hu6d301] 不在线，发送失败!");
        }
        return "send ok";
    }

}
