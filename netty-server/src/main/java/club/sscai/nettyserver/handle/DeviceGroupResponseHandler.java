package club.sscai.nettyserver.handle;

import club.sscai.nettyserver.protocol.request.DeviceGroupRequestPacket;
import club.sscai.nettyserver.protocol.response.DeviceGroupResponsePacket;
import club.sscai.nettyserver.util.SessionUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * 设备组消息处理了
 */
public class DeviceGroupResponseHandler extends SimpleChannelInboundHandler<DeviceGroupRequestPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DeviceGroupRequestPacket createGroupRequestPacket) {

//        List<String> userIdList = createGroupRequestPacket.getSnList();
//
//        List<String> snList = new ArrayList<>();
//        // 1. 创建一个 channel 分组
//        ChannelGroup channelGroup = new DefaultChannelGroup(ctx.executor());
//
//        // 2. 筛选出待加入群聊的用户的 channel 和 userName
//        for (String userId : userIdList) {
//            Channel channel = SessionUtil.getChannel(userId);
//            if (channel != null) {
//                channelGroup.add(channel);
//                userNameList.add(SessionUtil.getSession(channel).getSn());
//            }
//        }
//
//        // 3. 创建群聊创建结果的响应
//        String groupId = "RedWineCellar";
//        DeviceGroupResponsePacket createGroupResponsePacket = new DeviceGroupResponsePacket();
//        createGroupResponsePacket.setSuccess(true);
//        createGroupResponsePacket.setGroupId(groupId);
//        createGroupResponsePacket.setSnList(userNameList);
//
//        // 4. 给每个客户端发送拉群通知
//        channelGroup.writeAndFlush(createGroupResponsePacket);
//
//        System.out.print("群创建成功，id 为 " + createGroupResponsePacket.getGroupId() + ", ");
//        System.out.println("群里面有：" + createGroupResponsePacket.getUserNameList());
//
//        // 5. 保存群组相关的信息
//        SessionUtil.bindChannelGroup(groupId, channelGroup);

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("========================");
        System.out.println("========================");
        System.out.println("========================");
        System.out.println("========================");
        System.out.println("========================");
        // 1. 创建一个 channel 分组
//        ChannelGroup channelGroup = new DefaultChannelGroup(ctx.executor());
//
//        // 3. 创建群聊创建结果的响应
//        String groupId = "RedWineCellar";
//        DeviceGroupResponsePacket createGroupResponsePacket = new DeviceGroupResponsePacket();
//        createGroupResponsePacket.setSuccess(true);
//        createGroupResponsePacket.setGroupId(groupId);
//        createGroupResponsePacket.setSnList(userNameList);
    }
}
