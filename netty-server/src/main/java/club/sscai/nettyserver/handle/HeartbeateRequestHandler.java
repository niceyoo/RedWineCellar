package club.sscai.nettyserver.handle;

import club.sscai.nettyserver.protocol.request.HeartbeateRequestPacket;
import club.sscai.nettyserver.protocol.request.LoginRequestPacket;
import club.sscai.nettyserver.protocol.response.LoginResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Date;

public class HeartbeateRequestHandler extends SimpleChannelInboundHandler<HeartbeateRequestPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HeartbeateRequestPacket heartbeateRequestPacket) {
        System.out.println(new Date() + ": 收到客户端登录请求……");
        System.out.println(heartbeateRequestPacket.getMsg());;
    }

}
