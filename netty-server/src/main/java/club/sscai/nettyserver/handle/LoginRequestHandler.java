package club.sscai.nettyserver.handle;

import club.sscai.nettyserver.protocol.response.DeviceConfig;
import club.sscai.nettyserver.session.Session;
import club.sscai.nettyserver.util.SessionUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import club.sscai.nettyserver.protocol.request.LoginRequestPacket;
import club.sscai.nettyserver.protocol.response.LoginResponsePacket;
import java.util.Date;

/**
 * 登陆请求处理类
 */
public class LoginRequestHandler extends SimpleChannelInboundHandler<LoginRequestPacket> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginRequestPacket loginRequestPacket) {
        System.out.println(new Date() + ": 收到客户端登录请求……");

        LoginResponsePacket loginResponsePacket = new LoginResponsePacket();
        loginResponsePacket.setVersion(loginRequestPacket.getVersion());
        if (valid(loginRequestPacket)) {
            loginResponsePacket.setSuccess(true);
            System.out.println(new Date() + ": 登录成功!");
            // TODO 从数据库读取设备配置，如下直接写死了
            DeviceConfig deviceConfig = new DeviceConfig();
            deviceConfig.setName("红酒窖智能温控设备");
            deviceConfig.setDeviceMusicSize(6);
            deviceConfig.setLocation("济南市高新区中铁汇苑XXX楼XXX号");
            loginResponsePacket.setConfig(deviceConfig);
            loginResponsePacket.setServiceId("niceyoo");
            // 保存登陆会话
            SessionUtil.bindSession(new Session(loginRequestPacket.getSn()), ctx.channel());
        } else {
            loginResponsePacket.setReason("设备未注册");
            loginResponsePacket.setSuccess(false);
            System.out.println(new Date() + ": 登录失败!");
        }

        // 登录响应
        ctx.channel().writeAndFlush(loginResponsePacket);
    }

    /**
     * 验证客户端真实性
     * @param loginRequestPacket
     * @return
     */
    private boolean valid(LoginRequestPacket loginRequestPacket) {
        // 判断协议版本号
        if(loginRequestPacket.getVer() == 1){
            // 设备序列号
            String sn = loginRequestPacket.getSn();
            // 需要从数据库查询该设备是否入册
            return true;
        }
        return false;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        SessionUtil.unBindSession(ctx.channel());
    }
}
