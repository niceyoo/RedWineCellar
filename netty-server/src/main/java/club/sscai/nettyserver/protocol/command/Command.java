package club.sscai.nettyserver.protocol.command;

/**
 * 公共参数
 */
public interface Command {

    /**
     * 登陆请求
     */
    Byte LOGIN_REQUEST = 1;

    /**
     * 登陆回复
     */
    Byte LOGIN_RESPONSE = 2;

    /**
     * 心跳请求
     */
    Byte HEARTBEAT_REQUEST = 3;

    /**
     * 消息请求
     */
    Byte MESSAGE_REQUEST = 4;

    /**
     * 消息回复
     */
    Byte MESSAGE_RESPONSE = 5;

    /**
     * 设备群组请求
     */
    Byte DEVICE_GROUP_REQUEST = 7;

    /**
     * 设备群组回复
     */
    Byte DEVICE_GROUP_RESPONSE = 8;

    /**
     * 加群请求
     */
    Byte JOIN_GROUP_REQUEST = 9;

    /**
     * 加群回复
     */
    Byte JOIN_GROUP_RESPONSE = 10;


}
