package club.sscai.nettyserver.protocol.request;

import club.sscai.nettyserver.protocol.Packet;
import lombok.Data;

import java.util.List;
import static club.sscai.nettyserver.protocol.command.Command.DEVICE_GROUP_REQUEST;

@Data
public class DeviceGroupRequestPacket extends Packet {

    /**
     * 设备序列号列表
     */
    private List<String> snList;

    @Override
    public Byte getCommand() {
        return DEVICE_GROUP_REQUEST;
    }
}
