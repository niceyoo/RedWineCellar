package club.sscai.nettyserver.protocol.request;

import lombok.Data;
import club.sscai.nettyserver.protocol.Packet;

import static club.sscai.nettyserver.protocol.command.Command.JOIN_GROUP_REQUEST;

@Data
public class JoinGroupRequestPacket extends Packet {

    /**
     * 组id
     */
    private String groupId;

    @Override
    public Byte getCommand() {
        return JOIN_GROUP_REQUEST;
    }
}
