package club.sscai.nettyserver.protocol.request;

import club.sscai.nettyserver.protocol.Packet;
import lombok.Data;

import static club.sscai.nettyserver.protocol.command.Command.LOGIN_REQUEST;

/**
 * 登陆请求包
 */
@Data
public class LoginRequestPacket extends Packet {

    /**
     * 协议版本号，目前为止1
     */
    private Integer ver;

    /**
     * 设备序列号
     */
    private String sn;

    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}
