package club.sscai.nettyserver.protocol.request;

import club.sscai.nettyserver.protocol.Packet;
import lombok.Data;
import static club.sscai.nettyserver.protocol.command.Command.MESSAGE_REQUEST;

@Data
public class MessageRequestPacket extends Packet {

    /**
     * 设备序列号
     */
    private String sn;

    /**
     * 消息内容
     */
    private String message;

    @Override
    public Byte getCommand() {
        return MESSAGE_REQUEST;
    }
}
