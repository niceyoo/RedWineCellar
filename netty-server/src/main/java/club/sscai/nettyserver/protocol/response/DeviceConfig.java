package club.sscai.nettyserver.protocol.response;

import lombok.Data;

/**
 * 设备参数实体
 */
@Data
public class DeviceConfig {

    /**
     * 设备名称
     */
    private String name;

    /**
     * 设备安装位置
     */
    private String location;

    /**
     * 设备音量
     */
    private Integer deviceMusicSize;

}
