package club.sscai.nettyserver.protocol.response;

import club.sscai.nettyserver.protocol.Packet;
import lombok.Data;
import static club.sscai.nettyserver.protocol.command.Command.DEVICE_GROUP_RESPONSE;
import java.util.List;

@Data
public class DeviceGroupResponsePacket extends Packet {

    private boolean success;

    private String groupId;

    private List<String> snList;

    @Override
    public Byte getCommand() {
        return DEVICE_GROUP_RESPONSE;
    }
}
