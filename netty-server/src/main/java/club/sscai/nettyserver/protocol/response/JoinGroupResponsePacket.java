package club.sscai.nettyserver.protocol.response;

import lombok.Data;
import club.sscai.nettyserver.protocol.Packet;

import static club.sscai.nettyserver.protocol.command.Command.JOIN_GROUP_RESPONSE;

@Data
public class JoinGroupResponsePacket extends Packet {

    private String groupId;

    private boolean success;

    private String reason;

    @Override
    public Byte getCommand() {
        return JOIN_GROUP_RESPONSE;
    }
}
