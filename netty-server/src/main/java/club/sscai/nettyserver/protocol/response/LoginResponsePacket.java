package club.sscai.nettyserver.protocol.response;

import club.sscai.nettyserver.protocol.Packet;
import lombok.Data;

import static club.sscai.nettyserver.protocol.command.Command.LOGIN_RESPONSE;

@Data
public class LoginResponsePacket extends Packet {

    /**
     * 服务端id
     */
    private String serviceId;

    /**
     * 登陆成功与否
     */
    private boolean success;

    /**
     * 登陆失败原因
     */
    private String reason;

    /**
     * 设备参数，登陆成功后从数据库读取
     */
    private DeviceConfig config;

    @Override
    public Byte getCommand() {
        return LOGIN_RESPONSE;
    }
}
