package club.sscai.nettyserver.server;

import club.sscai.nettyserver.codec.PacketDecoder;
import club.sscai.nettyserver.codec.PacketEncoder;
import club.sscai.nettyserver.codec.SpliterFilter;
import club.sscai.nettyserver.handle.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 服务端代码
 */
@Component
public class NettyServer {

    private NioEventLoopGroup bossGroup = new NioEventLoopGroup();
    private NioEventLoopGroup workerGroup = new NioEventLoopGroup();

    @PostConstruct
    public void start() throws InterruptedException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap
                .group(bossGroup, workerGroup)

                // 指定Channel
                .channel(NioServerSocketChannel.class)

                //服务端可连接队列数,对应TCP/IP协议listen函数中backlog参数
                .option(ChannelOption.SO_BACKLOG, 1024)

                //设置TCP长连接,一般如果两个小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
                .childOption(ChannelOption.SO_KEEPALIVE, true)

                //将小的数据包包装成更大的帧进行传送，提高网络的负载
                .childOption(ChannelOption.TCP_NODELAY, true)

                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) {
                        // 消息协议过滤器
                        ch.pipeline()
                            .addLast(new SpliterFilter())
                            .addLast(new PacketDecoder())
                            .addLast(new LoginRequestHandler())
                            .addLast(new AuthHandler())
                            .addLast(new MessageRequestHandler())
                            .addLast(new HeartbeateRequestHandler())
                            .addLast(new PacketEncoder())
                            .addLast(new DeviceGroupResponseHandler());
                            //.addLast(new NettyServerHandler());
                    }
                });

        serverBootstrap.bind(8070);
    }

    @PreDestroy
    public void destory() throws InterruptedException {
        bossGroup.shutdownGracefully().sync();
        workerGroup.shutdownGracefully().sync();
//        log.info("关闭Netty");
    }

}
