package club.sscai.nettyserver.session;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Session {

    /**
     * 设备序列号
     */
    private String sn;

    public Session(String sn) {
        this.sn = sn;
    }

    @Override
    public String toString() {
        return "序列号：" + sn;
    }

}
